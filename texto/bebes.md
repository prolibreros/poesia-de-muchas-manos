---
breaks: false
tags: poesia, laura, antologia
title: Poesía de muchas manos
---

<figure>

![Ilustración en Mary LaFetra Russell, *Mother Goose*, Gabriel Sons & Company, Nueva York, 1917. (Foto: Pinterest / WOODNISSE)](https://gitlab.com/prolibreros/poesia-de-muchas-manos/-/raw/no-masters/portada/portada.small.png)

  <figcaption>

Ilustración en Mary LaFetra Russell, *Mother Goose*. Nueva York: Gabriel Sons & Company, 1917. Foto: Pinterest / WOODNISSE.

  </figcaption>
</figure>

# Prólogo

Un recuerdo de la infancia ha acompañado mi interés en la lírica tradicional infantil. Es la imagen de mi mamá pellizcando uno a uno los dedos extendidos de mi mano mientras pronuncia rítmicamente el «periquito mandurito».[^1] Recuerdo, sobre todo, la disposición mutua a la risa. Apenas duraba un instante esa forma brevísima de poesía, cuyo sentido no comprendía pero tampoco era incómodo ignorarlo. Bastaba saber que ella compartía conmigo el sillón, que había pausado sus faenas para dirigirme unas palabras juguetonas. Tiempo después, como estudiosa de la literatura, supe que ese momento encerraba una tradición poética. De alguna manera, las voces hispánicas de siglos pasados estaban allí perpetuándose.

Con este preámbulo personal, me dirijo al lector adulto para apelar a sus propios recuerdos. De modo que voltee la mirada a la etapa inicial de su relación con el lenguaje. Aunado a esto, un objetivo que motivó la hechura de esta antología es el de contribuir a la reflexión de la prevalencia literaria en contextos de crianza, donde el juego, el movimiento y la voz se imbrican. Aquí, la discusión sobre la dupla cultura-infancia se vuelve necesaria. Por ello, he elegido fragmentos de autoras que han encauzado su pensamiento a la oralidad en vinculación con aspectos interesantes para los tutores de niñ@s.

Comencemos con Ana Pelegrín (Argentina, 1938 - Madrid 2008) quien posicionó a los estudios de la tradición oral en un peldaño importante dentro del campo cultural. Su incansable compilación de juegos y tradiciones orales devino en la afirmación que estos son el modo literario básico para el niño pequeño, debido a la carga afectiva que contienen las palabras y los gestos al ser enunciados por una voz cercana. Por otra parte, en las postrimerías de su vida alcanzó a vislumbrar que las voces literarias eran acalladas por la intromisión de la tecnología audiovisual. Sin llegar a ser fatalista, confió en la capacidad crítica y creativa del niño para recrear sus tradiciones en nuevos contextos multimodales.

> La cultura infantil tradicional, oral, gestual, preciso es reconocerlo, evidencia señales de extinción. Este proceso, aunque acelerado, no significa una desaparición fulminante. Simplemente porque el proceso cultural no es producto de una generación, antes bien, lenta asimilación, ya que subsiste un mensaje relacional directo entre el hacer cultural y la vida de una comunidad.[^2]

He incluido también a Mercedes Calvo, escritora uruguaya, gran promotora de la poesía infantil. Ella destaca que la función lúdica del lenguaje se expresa en los juegos tradicionales y la poesía popular. Asimismo, en la comunicación afectiva se devela el umbral de la sensibilidad poética. Puesto que el bebé naturalmente explora los sonidos antes de interesarse por los significados, permitirle encuentros con la musicalidad es un aliciente para su participación activa, hasta que poco a poco cuestione el valor semántico.

Otra escritora contemporánea imprescindible en temas de cultura e infancia es la argentina María Emilia López. Veremos su concepción de literatura para la primera infancia, en la cual resuenan las reflexiones de Pelegrín; mas la contemporaneidad de López genera una lectura fresca y profunda gracias a los conceptos que propone. Si algo la caracteriza es su defensa de la ternura como contrapeso a la *precariedad vincular*, donde los duros condicionamientos sociales pesan sobre la infancia, debilitando la función de *jugador* mientras se legitima al niño como usuario pasivo de las tecnologías. De acuerdo con la autora, la relación inicial con la palabra imaginativa, con la metáfora,[^3] resulta imprescindible para afianzar las bases éticas y estéticas.

Por último, he agregado un fragmento del prólogo escrito por Mercedes Díaz Roig y María Teresa Miaja en la antología *Naranja dulce, limón partido. Antología de la lírica infantil mexicana* (Colegio de México, 1982). La cita seleccionada queda como invitación a unir fuerzas contra el olvido del folclor.[^4] Las recopiladoras no adoptan la actitud romántica de rescatar lo muerto, ni de considerar a lo viejo como lo más auténtico. No obstante, de cara al futuro de las generaciones más jóvenes, conceden valor a las actualizaciones y a la hibridez entre la tradición e innovación. Reivindican al folclor como el conjunto de expresiones populares vivas, adaptables a los giros modernos.

Espero que el lector pueda encontrar puentes entre los fragmentos teóricos de las distintas autoras. Como es de notarse, intercalé la teoría con las narraciones poéticas para destacar su función literaria y cultural. En consonancia, el segundo objetivo que persigue esta antología tiene que ver con la aplicación de poesía popular en los entornos donde suceden vínculos entre cuidadores y niñ@s de 0 a 6 años, margen de edad considerado como primera infancia. Podría ser de utilidad, por ejemplo, en hogares donde los adultos intentan sosegar a sus hijos a través de movimientos y sonidos; o en guarderías donde las cuidadoras *arropan* a bebés que recién experimentan su cuerpo desprendido del materno.

Es preciso ahora preguntarnos qué es la lírica tradicional. Retomo la definición elaborada por el investigador mexicano Roberto Rivelino García Baeza.

> “Lírica” se refiere al texto literario propiamente dicho en verso; “tradicional” a su forma de vida en variantes; e “infantil” al ámbito al que pertenece. Por tanto podemos entender como Lírica tradicional infantil al conjunto de textos que acompañan los juegos de los niños, quienes son receptores y emisores; los textos viven en constante variación y reelaboración; presentan distintos niveles de desarrollo musical, es decir, poseen elementos musicales que van desde la presencia del ritmo en su forma más simple hasta la estructuración de un esquema melódico desarrollado; y pueden ser de carácter puramente lírico o con elementos narrativos como los romances.[^5]

Ahora bien, si quiere asociarse a la lírica infantil con el concepto de literatura oral es pertinente ir con cuidado. La misma Ana Pelegrín resaltaba su contradicción, en tanto que la etimología *littera* apunta a la escritura. Cuando equiparamos lo oral con lo escrito se obvia el valor del contexto y los procesos colectivos en los que acontece la oralidad. En cambio, la escritura es un acto solitario protagonizado por la figura del autor individual, quien genera un texto, el cual una vez publicado podrá ser juzgado por sus cualidades estéticas internas desligadas incluso del contexto en que fue producido. A pesar de la polarización entre lo literario y lo oral, Pelegrín acepta el término como marco de la lírica infantil pues recuerda que las fronteras son flexibles. Pone de ejemplo a los escritores españoles que han nutrido sus obras de las fuentes orales.[^6]

En contraparte, el historiador y filósofo Walter Ong en su libro intitulado *Oralidad y escritura. Tecnologías de la palabra* (1982) se refirió al concepto de literatura oral con los adjetivos de monstruoso y absurdo puesto que denota la «incapacidad para revelar ante nuestro propio espíritu una herencia de material organizado en forma verbal salvo como cierta variante de la escritura, aunque no tenga nada en absoluto que ver con esta última» (p. 21). Ya que no podemos despojarnos de una mentalidad modelada por la cultura escrita (y sobre todo impresa), resulta fácil adjudicar el concepto de literatura a las formas verbales emanadas de la oralidad primaria, aquella propia de personas que no han tenido contacto con la escritura.

La disposición de los chicos frente a la oralidad difiere de la que tuvieron nuestros abuelos. Ahora son conscientes del poder de los dispositivos que los rodean. La voz materna compite con el sonido electrónico y al mismo tiempo, un aparato como un teléfono móvil resulta ser un auxiliar para subsanar las ausencias familiares. El niño puede tener cerca una voz entrañable mientras haya sido grabada. Pese a la tendencia de crianza que incita a mantener a los niños fuera del alcance tecnológico, creo que podemos potencializar el uso de dispositivos, en virtud de que cada vez más, nuestras prácticas sociales se afirman con el celular. 

La aceptación de la oralidad secundaria no demerita la función del cuerpo humano. El título *Poesía de muchas manos* focaliza el papel de la memoria colectiva y el cuerpo en la creación de un ambiente propicio para el lenguaje literario. He discurrido las clasificaciones: narraciones de cosquillas, narraciones de espacios, rimas y juegos sonoros, narraciones de sorpresas y canciones. Me gusta imaginar a los participantes de estas formas literarias como jugadores dispuestos a encarnar las estructuras poéticas que vienen de otras generaciones, de otras manos, al tiempo que contribuyen a su propia experiencia de juego. En otras palabras, más allá de los dispositivos, la performance[^7] activará la viveza poética. 

Entonces cabe preguntarse: ¿de verdad puede un bebé ser partícipe del juego? No son pocos quienes se muestran renuentes a la idea. «Ella todavía no entiende», dijo la abuela de Romina, una bebé de un mes de edad, al ver que su niñera improvisaba una historia con unos títeres de dedos. A pesar de que Romina reaccionaba atenta, fue relegada a los sonidos electrónicos que emanaban de su cuna, onomatopeyas sujetas a la vida útil de las baterías. Lo paradójico es que muchas veces al bebé se le veta la palabra viva y familiar, no para procurarle el silencio sino para ofrecerle la voz grabada y los estímulos electrónicos programados por un software.

¿Vale la pena dedicar la atención a esos humanos frágiles cuya expresión oscila entre el silencio, el llanto y la risa, pasando por el balbuceo? No olvidemos que todas esas formas del lenguaje primigenio, más cercano al sinsentido, han generado un interés especial en los poetas.[^8] Y pese a que el lector de estas páginas no se identifique con ellos, de acuerdo con los propósitos antes mencionados, propongo que se coloque en un espacio poético. No es tarea fácil porque implica despojarse, aunque sea momentáneamente, del pragmatismo que ha encasillado a la poesía como una manifestación inútil.

Será propicio pensar nuestra labor como mediadores de la literatura. Podemos comenzar focalizando aquellas manifestaciones poéticas que casi pasan desapercibidas en el flujo del lenguaje ordinario, preguntarnos por su origen, mayoritariamente hispánico, heredado y actualizado a través de las generaciones mexicanas. ¿Qué estamos contando o dejando de contar? ¿Cómo actualizamos la tradición ajustándola a nuestro presente caótico? Concido con Mercedes Calvo en la creencia de que la poesía es per se, un hecho pedagógico. Sin ser el poema un vehículo para llegar a otro fin, el poema «aumenta la capacidad de sentir, convirtiéndose así lo que pretende no ser más que materia de literatura, en materia de la vida».[^9]

Por último, quiero agradecer a las personas que donaron su tiempo para agitar su memoria en busca de los relatos que envolvieron su infancia. También a las amigas que accedieron a grabarse para evidenciar las posibilidades de movimiento a la hora de narrar. En los créditos dominan los nombres femeninos pero desde luego celebro que cada vez más los varones se inmiscuyan en tareas que han sido delegadas históricamente a la mujer. No entraremos en discusiones de género; sin embargo, subyace el deseo de que los padres se sumen al diálogo sobre la dimensión afectiva de la palabra. La responsabilidad con la infancia requiere de todos los géneros y de todos los esfuerzos.

[^1]: La versión de mi mamá asemeja con la recopilada en *Naranja dulce, limón partido. Antología de la lírica infantil mexicana*. Ciudad de México: Colegio de México, 1982: Pico, pico, madurico/tú que vas, tú que vienes/a lavar las mantillitas/a la gata marifata/Alza la mano/cuchara de plata. En cambio, Margit Frenk en *El folklore poético de los niños mexicanos* (1973) recupera una versión dialógica y más explícitamente religiosa: Periquito mandurico, ¿quién te dio tan grande pico?/Mi señor Jesucristo/Tú que vas, tú que vienes de lavar los manteles de la chata narigata, ¡esconde la mano detrás de esa cruz de plata!

[^2]: Pelegrín, Ana. *Cada cual atienda su juego: de tradición oral y literatura.* Alicante: Biblioteca Virtual Miguel de Cervantes, 2008. Edición digital basada en la de Madrid, Cincel, 1986.

[^3]: Mercedes Calvo también otorga atención a la metáfora: «Y aunque en su esdrújulo nombre la metáfora pretenda asustarnos con su disfraz de figura retórica, sabemos que no es más que una palabra balanceándose suavemente entre dos sentidos, como un niño en su columpio». *Poesía con niños. Guía para propiciar el encuentro de los niños con la poesía.* Ciudad de México: Coordinación Nacional de Desarrollo Cultural Infantil, 2012, p.17.

[^4]: Concepto que nos llega del inglés *folklore*. Designa el conjunto de saberes, creencias, artesanías y prácticas orales de carácter tradicional. Néstor García Canclini, reconocido investigador de las culturas populares, explica en su libro *Culturas híbridas. Estrategias para entrar y salir de la modernidad*. México: Random House Mondadori, 2009, p.198, que el estudio del folclor nació en Europa durante el siglo XIX y luego se extendió a América con el mismo ímpetu melancólico de responder a «la ceguera aristocrática y como réplica a la primera industrialización de la cultura». Advierte la falta de cuestionamiento por parte de los folcloristas sobre los procesos adaptativos de las culturas populares frente a las culturas de masas. En ese sentido, podemos preguntarnos cómo los actuales *mass media* beben de la tradición oral. Plataformas como Youtube y Facebook son espacios de difusión para proyectos como *Nican Nican*, dirigido por la mexicana Ana Prado, quien en su tarea de acercar el lenguaje artístico a la primera infancia ha actualizado ciertos juegos de manos o cuentos de regazo tradicionales.

[^5]: Tesis de maestría albergada en el repositorio del Colegio de San Luis, A.C.: “La Lírica tradicional infantil de México: letra y música”, p. 12. Además, podemos encontrar allí referencias a importantes compilaciones que se han hecho en nuestro país, destacando la ya mencionada de Margit Frenk y la de Vicente T. Mendoza: *Lírica infantil de México* (1951).

[^6]:«Los polos rebajan su distancia, la oposición pierde contornos tan netos, en varios momentos claves de la literatura española, en el gusto por los temas y formas populares del hombre del Renacimiento, posteriormente del siglo XIX, de la generación del 98, el neopopularismo de la del 27: Juan del Encina, Cervantes, Lope, Tirso, García Lorca, Alberti, por no citar sino a los archicitados autores en cualquier manual literario» *La aventura de oír: cuentos y memorias de tradición oral.* Alicante: Biblioteca Virtual Miguel de Cervantes, 2008. Edición digital basada en la de Madrid, Cincel, 1986

[^7]: Recupero el tratamiento de la performance de Roberto Rivelino García Baeza, Íbid. «Los textos y las instrucciones de juegos y canciones que se registran y catalogan tienen una función de conservación y –de alguna manera- de trasmisión; sin embargo, el juego y la canción van a vivir cuando se ejecuten en la performance. Sólo así existirán de manera íntegra, real. La música, el texto y el movimiento son los elementos constitutivos naturales de la lírica tradicional infantil.» (p. 5)

[^8]: Pensemos en el movimiento dadaísta, iniciado en Suiza en 1916, el cual rehuía de los convencionalismos literarios y artísticos en busca de un lenguaje fresco y vanguardista. Aunque no hay consenso en el origen del término dadá, uno de los supuestos es que se inspira en el balbuceo de los bebés.

[^9]: *Poesía con niños. Guía para propiciar el encuentro de los niños con la poesía.* Ciudad de México: Coordinación Nacional de Desarrollo Cultural Infantil, 2012, p.28.

# 1. Narraciones de cosquillas

> Para el niño pequeño, la palabra oída ejerce una gran fascinación. La palabra y su tonalidad, su ritmo, los trazos afectivos que teje la voz, cuando es temperatura emocional, calma, consuelo, ternura, sensorialidad latente. El magnetismo por el ritmo y la entonación puede desplegarse con intensidad, al escuchar la voz de otras memorias, viejas-nuevas-voces, de rimas, retahílas,[^10] cancioncillas, cuentos.

[^10]: Definidas por la misma autora como la «poesía lúdica, de escasos o múltiples elementos; el decir poético de los niños, de escasa y secreta comprensión lógica. La palabra acompaña al juego, convirtiéndose ella misma en juego, y es tratada como un juguete rítmico oral, dando paso a libres asociaciones fónicas. Es un repertorio heterogéneo pero comparte estructuras y motivos con otras ramas de la cultura popular europea: las *comptines* francesas, las *nursery rhymes* inglesas, las *lengas lengas* portuguesas, o las *filastroche* italianas». Pelegrín, Ana. Retahílas tradicionales: el sentido del sinsentido. Alicante : Biblioteca Virtual Miguel de Cervantes, 2009.

> Podemos formular la hipótesis de que la literatura oral es una forma básica, un modo literario esencial en la vida del niño pequeño, porque la palabra está impregnada de afectividad. El cuento, el romance, la lírica, construyen el mundo auditivo-literario del niño, le incorporan vivencialmente a una cultura que le pertenece, le hacen partícipe de una creación colectiva, le otorgan signos de identidad. El libro oído, visto, tocado, olido, el desciframiento emocional-oral-sensorial, el libro-lectura compartida con otro, le ayudarán en su contacto posterior con la letra impresa, motivando una lectura gozosa.

> No olvidemos que la manipulación de los medios de comunicación, los imperativos comerciales del niño consumidor, hoy por hoy, influyen en su mundo cotidiano, y son realidades cuantitativamente inapelables. Pero invitamos a reflexionar sobre la participación activa, crítica, en la producción literaria, del niño receptor, re-creador, transmisor-autor, o meramente jugador-gozador, lector creativo.

Fragmento de Pelegrín, Ana. *La aventura de oír: cuentos y memorias de tradición oral*. Alicante: Biblioteca Virtual Miguel de Cervantes, 2008. Edición digital basada en la de Madrid, Cincel, 1986.

## El gusanito

Hay un gusanito buscando su cuevita y ya la encontró.

## El conejito

Aquí anda un conejito, comiendo **zacatito**, llegó la **lluvinita** y que corre a su cuevita.

Compartidos por Gabriela Beltrán, vecina de Cofradía de Suchitlán, Comala, Colima.

Ve el movimiento aquí:

<figure>

![Participaron en el video: Rosy Orozco y Lilik Demouy.](https://gitlab.com/prolibreros/poesia-de-muchas-manos/-/raw/no-masters/qr/01_narraciones_de_cosquillas.url.png)

  <figcaption>

Participaron en el video: Rosy Orozco y Lilik Demouy.

  </figcaption>
</figure>

## Glosario

Zacatito : Diminutivo de zacate, que proviene del náhuatl *zacatl* y significa pasto o hierba.

Lluvinita : Variante del diminutivo lluviecita.

# 2. Narraciones de espacios

> Las primeras palabras aprendidas por el niño tienen el encanto del sonido, cuyo goce antecede al significado. Muchos niños, al escuchar por primera vez una palabra, no inquieren de inmediato por su significado, sino que juegan largamente con su sonido, susurrándolo, diciéndolo en alta voz, con diferentes entonaciones. Después le inventan significados diversos, poniendo la palabra en nuevos contextos, para ir, mucho después, ya agotado el juego, en busca de la definición.

> Apresurarnos a fijar el significado de las palabras nuevas cuando el niño aún no lo ha preguntado tal vez contribuya a ampliar su nivel lingüístico, pero es empobrecedor en otros aspectos sustanciales. Porque el lenguaje tiene también una función lúdica, función que se recrea, no sólo en el habla infantil sino también en la poesía folclórica, los juegos tradicionales, etcétera.

Fragmento de Calvo, Mercedes. *Poesía con niños. Guía para propiciar el encuentro de los niños con la poesía*. Ciudad de México: Coordinación Nacional de Desarrollo Cultural Infantil, 2012.

## Hogares

En este nidito vive un pajarito. \
La abeja se esconde en este panal. \
El listo conejo se esconde en su agujero. Sh, sh, sh… \
Y yo en mi camita me voy a acostar.

Compartido por Elena Altamirano Vásquez, vecina de Oaxaca, México.

## Recorrido de la enana

Mira a esta enana cómo sube la escalera, \
toca el timbre, \
toca la puerta, \
¡Buen día enanita!

La adaptación al español es mía, basada en la versión alemana compartida por Martha y Wilfried Flaig: Es geht ein Mann die Treppe hoch, klingelt, klopf an, guten Tan kleine Mann. En el original se menciona a un hombrecito que sube la escalera. He preferido el sujeto «enanita» puesto que tiene mayor carga afectiva en mi historia familiar. Podemos cambiar palabras siempre y cuando no afectemos la musicalidad.

Ve el movimiento aquí:

<figure>

![Participaron en el video: Rosy Orozco, Hafiz León y Lilik Demouy.](https://gitlab.com/prolibreros/poesia-de-muchas-manos/-/raw/no-masters/qr/02_narraciones_de_espacios.url.png)

  <figcaption>

Participaron en el video: Rosy Orozco, Hafiz León y Lilik Demouy.

  </figcaption>
</figure>

# 3. Rimas y juegos sonoros

> Si queremos formar niños sensibles al lenguaje poético empezaremos, sin duda, cuando es bebé, con las canciones de cuna. O aun antes: se ha demostrado que el feto es sensible al ritmo y a la voz humana [...] No importa, entonces, el texto que se cante, por lo menos, no importa para el niño. Este es sensible, antes que a la palabra, al aspecto sonoro del poema, y lo que prima aquí es la comunicación afectiva. En efecto, la palabra aún no tiene valor semántico, importa exclusivamente lo sonoro. Poesía y música van estrechamente enlazadas hasta aproximadamente los dos años.

> Después, paulatinamente, el niño va descubriendo, en ese conjunto de palabras, algunas que, para él, tienen una significación especial (mamá, nene, su nombre) y adjudica ese significado a todo el poema, sin distinguir más detalles. Alrededor de los dos años y medio el niño va superando esta etapa y el poema va siendo concebido como una unidad. Aunque lo corporal sigue siendo importante, comienza a actuar sobre las palabras, desordenándolas, repitiéndolas, en un juego que se extiende hasta los tres años aproximadamente.

> Es la época de los juegos de dedos y los sencillos poemas narrativos.

Fragmento de Calvo, Mercedes. *Poesía con niños. Guía para propiciar el encuentro de los niños con la poesía*. Ciudad de México: Coordinación Nacional de Desarrollo Cultural Infantil, 2012.

## Pon, pon, tata

Pon, pon, tata, \
**mediecito** pa la papa; \
pon, pon, pon, \
mediecito pal jabón.

## Pica perica

Pica, pica, pica, perica. \
Pica, pica, pica la rama.

Compartida por Silvia Andrade, vecina de Colima.

## Tengo manita

Tengo manita, \
no tengo manita, \
porque la tengo \
desconchabadita.

Compartida por Martha Flaig, oriunda de México. Actualmente vive en Alemania.

En *Naranja dulce, limón partido* aparece esta versión:

No tengo manita, \
no tengo manita, \
porque la tengo \
desconchinfladita.

## Tortillitas

Tortillitas de manteca \
pa mamá que está contenta; \
tortillitas de maíz \
pa mamá que está feliz; \
tortillitas de cebada \
pa mamá que está enojada

Compartida por Martha Flaig, oriunda de México. Actualmente vive en Alemania.

En *Naranja Dulce, Limón Partido* aparece esta versión:

Tortillitas de manteca \
para mamá que está contenta; \
tortillitas de salvado \
para papá que está enojado.

Mercedes Calvo en *Poesía con niños* (2012) recoge esta versión:

Tortitas de manteca \
para mamá que da la teta; \
tortitas de cebada \
para papá que no da nada. \
Tortitas de polvorones, \
para abuelita que da coscorrones.

Ve el movimiento aquí:

<figure>

![Participaron en el video: Silvia Andrade, Isis Rodríguez, Germán Romero y Dharita.](https://gitlab.com/prolibreros/poesia-de-muchas-manos/-/raw/no-masters/qr/03_rimas_y_juegos_sonoros.url.png)

  <figcaption>

Participaron en el video: Silvia Andrade, Isis Rodríguez, Germán Romero y Dharita.

  </figcaption>
</figure>

## Glosario

Desconchabadita : Descompuesta. Viene del verbo desconchabar usado en México, Cuba, América Central y Chile.

Desconchinfladita : Deteriorada. Viene del verbo desconchinflar que se usa en México, El Salvador, Cuba y R. Dominicana.

Mediecito : Diminutivo de medio, moneda que en la época colonial valía la mitad de un real. De esta manera el indicativo «pon» se dirige a una figura paterna (tata) con función de proveer alimento y productos básicos (papa y jabón).

# 4. Narraciones de sorpresas

> ¿Qué es la literatura para un bebé o niño pequeño?

> Eco / voz / palabra / imagen / literatura: desde las primeras palabras, esas que significan todo, esas que balbucean sentidos, esas que pesan más por la voz que las porta que por su etimología, esas que son eco del amor, de la ternura, esas que hacen de espejo, las que nos dicen / le dicen dónde está la belleza, que él / ella es la belleza, que la belleza y el amor son una sola cosa.

> Los sonidos: la voz que imprime sentidos a sus sensaciones, a sus dolores, a su hambre, a sus alegrías: la voz o las voces que le cantan canciones de manos, de ratones, de caballos, o canciones para dormir. Y claro que allí hay arte: en la poesía de la letra de cada canción, en las melodías, en las modulaciones, en el gesto corporal acunador, en el movimiento. Hay experiencias de placer, hay experiencias artísticas, hay un reservorio cultural en danza que el niño incorpora en su percepción amplia y dispuesta.

> Las imágenes: primero el rostro más querido, después otros rostros, más allá los colores, la naturaleza, los juguetes y finalmente los libros.

> «Protoliteratura» me gusta llamarla, esa que nace en las nanas, esa que crea una envoltura sonora capaz de contener y acompañar, esa que hace subjetividad y además estimula el juego, ese juego que poco a poco, en el encuentro con ciertos objetos de la cultura, comenzará a discriminarse como arte, aunque en un principio ambas experiencias estén totalmente imbricadas (...) Imaginar el alcance de la capacidad creadora de un niño pequeño sometido a tantos condicionamientos culturales como los de nuestro tiempo es bastante complejo, sobre todo si la oferta lo convierte cada vez más en «usuario» y menos en «jugador». Estas complejidades de época que se le suman al juego no son poca cosa, más aún si advertimos la precariedad vincular actual, y volvemos a la relación inicial entre cuidados maternantes, creatividad y posibilidad de creación artística. No hay arte sin metáfora, ni tampoco acceso al pensamiento sin metáfora. Contra lo plano y lo duro de la vida tecnologizada, el acceso a la ternura, a la palabra, a la vida imaginativa, transforma ética y estéticamente el modo de estar en el mundo.

Fragmento de López, María Emilia. «Arte y juego en los niños pequeños. Metáforas del vivir». *Conafecto*, 2017.

## Pobre gusanito

---¿Qué tienes allí? \
--- Un gusanito. \
--- ¿Con qué lo alimentas? \
--- Con pan y quesito. \
--- ¿Lo mataremos? \
--- No, pobrecito.

Compartida por Silvia Andrade vecina de Colima, Colima. En *Naranja Dulce, Limón Partido* aparece una versión con variaciones en palabras y extensión:

--- ¿Qué tienes aquí? \
--- Un gusanito. \
--- ¿Con qué lo mantienes? \
--- Con pan y quesito. \
--- ¿Con qué le das agua? \
--- Con un botecito. \
--- ¿Lo mataremos? \
--- ¡Ay no!, pobrecito.

Ve el movimiento aquí:

<figure>

![Participaron en el video: Silvia Andrade y Mariel Quirino.](https://gitlab.com/prolibreros/poesia-de-muchas-manos/-/raw/no-masters/qr/04_narraciones_de_sorpresas.url.png)

  <figcaption>

Participaron en el video: Silvia Andrade y Mariel Quirino.

  </figcaption>
</figure>

# 5. Canciones

> A través de las canciones tradicionales el niño absorbe una fuerte dosis del estilo, estructuras y entorno populares que refuerzan su arraigo cultural. Debemos pues procurar que nuestro folklore no sea anulado por otras fuerzas, sino que se una a ellas para formar nuestro futuro, un futuro semejante al de otros pueblos, pero no exactamente igual, un futuro donde se amalgamen lo nuevo y lo viejo, lo propio y lo ajeno. Solo conservando lo que fuimos podremos ser.

Fragmento de Díaz Roig, Mercedes y María Teresa Miaja. *Naranja dulce, limón partido. Antología de la lírica infantil mexicana*. Ciudad de México: El Colegio de México, 1982.

## De primavera

Avena, avena, avena, nos trae la primavera (2) \
Mi papacito la siembra aquí, \
mi mamacita la siembra allá. \
La recogen a veces así, a veces asá. \
Suenan las manos, suenan los pies. \
Dan una vuelta con rapidez.

Compartida por Silvia Andrade vecina de Colima, Colima.

## Huitzi araña

Huitzi, huitzi araña subió a su telaraña \
Vino la lluvia y se la tiró…poooing \
Salió el sol \
Se secó la lluvia \
Y Huitzi, huitzi araña, otra vez subió.

Compartida por Martha Flaig, oriunda de México. Actualmente vive en Alemania.

Ve el movimiento aquí:

<figure>

![Participaronen el video: Mariel Quirino, Isis Rodríguez y Dharita.](https://gitlab.com/prolibreros/poesia-de-muchas-manos/-/raw/no-masters/qr/05_canciones.url.png)

  <figcaption>

Participaron en el video: Mariel Quirino, Isis Rodríguez y Dharita.

  </figcaption>
</figure>